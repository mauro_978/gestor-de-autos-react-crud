const url = 'http://localhost:3004/';
const headers = {
    'Content-Type': 'application/json'
  };

class DataService {
    async getCars() {
        try {
            return fetch(`${url}autos`, {method: 'GET', headers })
        } catch (error) {
            console.error(error);
        }
    }
    async getBrands() {
        try {
            return fetch(`${url}marcas`, {method: 'GET', headers})
        } catch (error) {
            console.error(error);
        }
    }
    async createCar(data) {
        try {
            return fetch(`${url}autos`, {method: 'POST', headers, body: JSON.stringify(data) })
        } catch (error) {
            console.error(error);
        }
    }

    async updateCar(carId, data) {
        try {
            return fetch(`${url}autos/${carId}`, {method: 'PUT', headers, body: JSON.stringify(data) })
        } catch (error) {
            console.error(error);
        }
    }

    async deleteCar(carId) {
        try {
            return fetch(`${url}autos/${carId}`, {method: 'DELETE', headers})
        } catch (error) {
            console.error(error);
        }
    }
}

export default new DataService();