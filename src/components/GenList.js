import React from "react";

// A generic list that shows a certain amount of elements and its data
export default function GenList(props) {
    const {elements, headers, handleClick, selectedItem} = props;


    //map all headers of the list
    let JSXHeaders = headers.map((value, index) =>{
        return(<th key={index}>{value}</th>)
    })

    let JSXElements = <tr>Loading...</tr>; // if the elements are not loadead
    
    if(elements.length !== 0){
        JSXElements = elements.map((elm) => {
            let values = Object.values(elm);    //map all values of an element in an array
            values.pop();                     //generic array of values without the last element, the id
            let JSXValues = values.map((value, index) =>{
                return(<td key={index}>{value}</td>);
            })
            return (<tr key={elm.id} onClick={()=>handleClick(elm)} className={"gen-list-element " + (elm.id === selectedItem ? "selected": "unselected")}> 
                {JSXValues}
            </tr>)
        });    
    }
    
    return(
        <div className="gen-list-div">
            <table className="gen-list-table">
                <thead>
                    <tr>
                       {JSXHeaders}
                    </tr>
                </thead>
                <tbody>
                    {JSXElements}
                </tbody>
            </table>
        </div>
    );
}