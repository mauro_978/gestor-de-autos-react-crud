import React from "react";

export default function CarForm(props){
    const {carData, handleChange, handleSubmit, brands, handleClose} = props;

    let JSXMarcas = <option value="default">default</option>;

    if(brands.length !== 0){
        JSXMarcas = brands.map((value, index) => {
            return(<option value={value.nombre} key={index}>{value.nombre}</option>)
        })
    }
    return(  
        <div className="car-form-container"> 
            <div className="close-btn-container">
                <button className="close-btn" onClick={handleClose}>X</button>
            </div>
            <form className="car-form" onSubmit={handleSubmit}>
                <select 
                    id="marca" 
                    value={carData.marca}
                    onChange={handleChange}
                    name="marca"
                >   <option value={""} disabled defaultValue hidden>Marca</option>
                    {JSXMarcas}
                </select>
                <input
                    type="text"
                    placeholder="Modelo"
                    onChange={handleChange}
                    name="modelo"
                    value={carData.modelo}
                />
                <input
                    type="text"
                    placeholder="Color"
                    onChange={handleChange}
                    name="color"
                    value={carData.color}
                />
                <input
                    type="text"
                    placeholder="Patente"
                    onChange={handleChange}
                    name="patente"
                    value={carData.patente}
                />

                <button 
                    className="btn-accept"
                >
                    Aceptar
                </button>
            </form>
        </div>
    )
}