import GenList from "./GenList";
import CarForm from "./CarForm";
import DataService from "../services/dataService";
import { useState, useEffect } from "react";
import dataService from "../services/dataService";

const CARHEADERS = ['Marca', 'Modelo', 'Color', 'Patente'];

export default function App() {
  const [cars, setCars] = useState([]);
  const [newCarFormData, setNewCarFormData] = useState({
    marca: "",
    modelo: "",
    color: "",
    patente: ""
  });
  const [selectedCarData, setSelectedCarData] = useState({
    marca: "",
    modelo: "",
    color: "",
    patente: "",
    id:""
  });
  const [brands, setBrands] = useState([]);
  const [showNewCarForm, setShowNewCarForm] = useState(false);
  const [showUpdateCarForm, setShowUpdateCarForm] = useState(false);
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  
  //fetch all cars from the API
  const fetchCars = async () => {
    const res = await DataService.getCars();
    const resCars = await res.json();
    setCars(resCars)
  };

  useEffect(() => {
    fetchCars();
  }, [])

  //fetch all the brands from the API
  useEffect(() => {
    const fetchBrands = async () => {
      const res = await DataService.getBrands();
      const resBrands = await res.json();
      setBrands(resBrands);
    };
    fetchBrands();
  },[])

  //controller of the new car form inputs
  function handleNewFormChange(event) {
    const {name, value, type, checked} = event.target
    setNewCarFormData(prevFormData => {
        return {
            ...prevFormData,
            [name]: type === "checkbox" ? checked : value
        } 
    })
  }
  //controller of the update car form inputs
  function handleUpdateFormChange(event) {
    const {name, value, type, checked} = event.target
    setSelectedCarData(prevFormData => {
        return {
            ...prevFormData,
            [name]: type === "checkbox" ? checked : value
        } 
    })
  }

  // ---- Create, update, delete handlers ----
  function handleSubmitNewCar(event){
    event.preventDefault();
    if( newCarFormData.marca    !== "" &&
        newCarFormData.modelo   !== "" &&
        newCarFormData.color    !== "" &&
        newCarFormData.patente  !== ""){
          dataService.createCar(newCarFormData);
          closeNewCarForm();
          fetchCars();
          return;
    }
    alert("Necesita completar todos los campos para continuar");
  }

  function handleSubmitUpdateCar(event){
    event.preventDefault();
    if( selectedCarData.marca    !== "" &&
        selectedCarData.modelo   !== "" &&
        selectedCarData.color    !== "" &&
        selectedCarData.patente  !== ""){
          dataService.updateCar(selectedCarData.id, selectedCarData);
          closeUpdateCarForm();
          fetchCars();
          return;
    }
    alert("Necesita completar todos los campos para continuar");
  }

  function handleSubmitDeleteCar(){
    dataService.deleteCar(selectedCarData.id);
    setSelectedCarData({
      marca: "",
      modelo: "",
      color: "",
      patente: "",
      id:""
    });
    setShowDeleteConfirmation(false);
    fetchCars();
  }

  // ---- Modal windows functions ----

  function openNewCarForm(){
    setShowNewCarForm(true);
  }
  function closeNewCarForm(){
    setShowNewCarForm(false);
  }

  function openUpdateCarForm(){
    if(selectedCarData.id === ""){
      alert("Seleccione un auto primero");
      return;
    }
    setShowUpdateCarForm(true);
  }
  function closeUpdateCarForm(){
    setShowUpdateCarForm(false);
  }


  function openDeleteConfirmation(event){
    event.preventDefault();
    if(selectedCarData.id === ""){
      alert("Seleccione un auto primero");
      return;
    }
    setShowDeleteConfirmation(true);
  }
  function closeDeleteConfirmation(){
    setShowDeleteConfirmation(false);
  }

  function selectCar(data){
    setSelectedCarData(data);
    console.log(newCarFormData);
  }

  return (
    <div className="main">
      <nav className="top-navbar">
        <div className="top-navbar-name">
          <h1>Administracion de Autos</h1>
        </div>
      </nav>
      <div className="car-list-containter">
        <div className="btn-container">
          <button onClick={openNewCarForm} className="btn-accept">+ Agregar</button>
          <div className="secondary-btn-container">
            <button onClick={openUpdateCarForm} className="btn-accept">Editar</button>
            <button onClick={openDeleteConfirmation} className="btn-delete">Eliminar</button>
          </div>
        </div>
          <GenList 
            elements={cars} 
            headers={CARHEADERS} 
            handleClick={selectCar} 
            selectedItem ={selectedCarData.id} 
            key="1"
          />
      </div>

      {showNewCarForm && <div className="modal-window">
          <CarForm 
            carData={newCarFormData} 
            handleChange={handleNewFormChange}
            handleSubmit={handleSubmitNewCar}
            brands={brands}
            handleClose={closeNewCarForm}
          />
        </div>}
      {showUpdateCarForm && <div className="modal-window">
          <CarForm
            carData={selectedCarData}
            handleChange={handleUpdateFormChange}
            handleSubmit={handleSubmitUpdateCar}
            brands={brands}
            handleClose={closeUpdateCarForm}
          />
        </div>}
        {showDeleteConfirmation && <div className="modal-window">
        <div className="delete-confirmation-container">
          <div className="close-btn-container">
              <button className="close-btn" onClick={closeDeleteConfirmation}>X</button>
            </div>
            <h3>¿Esta seguro que desea eliminar este auto?</h3> 
            <div className="btn-container-delete-confirmation">
              <button className="btn-delete" onClick={handleSubmitDeleteCar}>Eliminar</button>
              <button className="btn-accept" onClick={closeDeleteConfirmation}>Cancelar</button>
            </div>
          </div>
        </div>}
    </div>
  );
}
