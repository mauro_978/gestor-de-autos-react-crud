# Aplicacion de gestion de Autos en React

Este projecto utiliza React como FrontEnd y [JSON-server](https://github.com/typicode/json-server) como BackEnd mock. 

## Instrucciones de Instalación

Instalar Git y Node para el manejo de dependencias.

Clonar el repositorio:

### `git clone https://gitlab.com/mauro_978/gestor-de-autos-react-crud`

Dentro del directorio raiz del projecto, instalar las dependencias de React ejecutando:
### `npm install`

Luego instalar JSON-server:
### `npm install -g json-server`

Desde el directorio raiz del projecto ejecutar JSON server de la siguiente manera:
```
cd .\BackEnd
json-server --watch db.json --port 3004
```
Por último, en otra terminal, desde el directorio raiz ejecutar la aplicacion con:
### `npm start`

Con esto la aplicación esta funcionando en [http://localhost:3000](http://localhost:3000)
